let arr = [1, 2, 3, 'hey', [1, 2, 3]];

function calc(data, symbol) {
  if (
    (Array.isArray(data) && symbol == '+') ||
    symbol == '-' ||
    symbol == '*' ||
    symbol == '/'
  ) {
    let x = data.flat(Infinity);
    let n = [];
    for (let i = 0; i < x.length; i++) {
      if (typeof x[i] == 'number') {
        n.push(x[i]);
      }
    }
    return eval(n.join(symbol));
  }
  return 'Only Array';
}

console.log(calc(arr, '+'));

// DOM Document Object Model
// document.getElementById();
// document.getElementsByClassName();
// document.getElementsByTagName();

// document.querySelector();
// document.querySelectorAll();

// const elem = document.getElementById('box');
// const el = document.getElementsByClassName('box');
// const tag = document.getElementsByTagName('div');

// el[0].style.width = '200px';
// el[0].style.height = '200px';
// el[0].style.backgroundColor = 'red';
// el[0].style.borderRadius = '50%';

// tag[3].style.cssText = 'backgroud-color: "red" ; width: 250px; height: 250px';
// elem.style = 'background-color: #080; width:250px ; height:250px';

// box.style = 'background-color: #080; width:250px ; height:250px';

// const box = document.querySelector('#box');
// const box1 = document.querySelectorAll('.box');

// const colors = [
//   'red',
//   'blue',
//   'violet',
//   'green',
//   'linear-gradient(red 33.3%, blue 33.3% 66.6%, orange 66.6%)',
// ];

// for (let i = 0; i < box1.length; i++) {
//   box1[
//     i
//   ].style = ` background: ${colors[i]}; width:200px ; height:200px; margin: 2px; display: inline-block`;
// }

//Events

onclick;
ondblclick;
oncontextmenu; //right click
onwheel; // mouse scroll
onmousewheel; // mouse scroll
onwheel;
onmouseover;
onmouseleave;
onmouseenter;
onmouseout;
onauxclick;
onmousedown;
onmouseup;
onmousemove;
onchange;
oninput;
onfocus;
onblur;

const box = document.querySelector('#box');
box.style =
  'background-color: #080; width:250px ; height:250px; transition: .5s';

// box.onclick = function () {
//   box.style.width = '500px';
//   box.style.background = 'orange';
// };

// box.ondblclick = () => {
//   box.style.borderRadius = '50%';
//   let x = [5, 'Hi', 'js'];
//   console.log(x.join('*'));
// };

// box.oncontextmenu = function () {
//   box.style.borderRadius = '50% 0';
//   return false;
// };

// box.oncontextmenu = () => {
//   return false;
// };

// box.oncontextmenu = () => false;

// box.oncontextmenu = function (event) {
//   box.style.borderRadius = '50% 0';
//   event.preventDefault();
// };

// document.oncontextmenu = (e) => e.preventDefault();

// let x = 250;
// box.onmousewheel = () => {
//   console.log(x);
//   x += 5;
//   box.style.width = x + 'px';
// };

// box.onclick = function () {
//   box.style.width = '500px';
//   box.style.background = 'orange';
//   box.onclick = function () {
//     box.style.width = '250px';
//     box.style.background = 'red';
//   };
// };

// box.ondblclick = () => {
//   box.style.borderRadius = '50%';
//   let x = [5, 'Hi', 'js'];
//   console.log(x.join('*'));
//   box.ondblclick = function () {
//     box.style.width = '250px';
//     box.style.background = 'red';
//   };
// };
