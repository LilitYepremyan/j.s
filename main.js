//Js Core DOM BOM OOP
// Document Object Model
//Browser Object Model
//Object Oriented Programming

// Number
// String
// Boolean true false
// undefined
// null
// Array
// Function
// Object

// let a = 1;
// let b = 2;
// document.writeln(a);

// document.write(a, b);

// let num = 52;

// document.writeln(num);

// const a = 20;
// document.write(a);

// var a = 10;

// document.write(a);

// + - / * %

// let a = 10;
// let b = 20;
// let result = a + b;
// let result = b - a;
// let result = a * b;
// let result = b / a;
// let result = b % a;

// let result = a + b * b;
// document.write(result);

{
  /* < > >= <= ==  === != !== */
}

// let a = 10;
// let b = 20;
// let res = a > b;
// let res = a < b;
// let res = b < a;
// let res = a >= b;
// let res = b <= a;
// let res = a == b;
// let res = a === b;
// let res = a != b;
// let res = a !== b;

// document.write(res);

// && || !

// let a = 5;
// let b = 3;
// let c = 9;
// let d = 17;

// let res = a > b && c < d;
// let res = a > b && c < d && d > b && c > b && a > c;

// let res = a > b || c > b;

// let a = true;

// document.write(!a);

// let boolean = !true;

// document.write(a);

// let a = 5;
// let b = 6;
// let c;

// c = b;
// b = a;
// a = c;

// c = b % a;
// a = a + c;
// b = b - c;

// a = a + 1;
// b = b - 1;

// a = a + b;
// b = a - b;
// a = a - b;

// document.write(a);
// document.write(b);

//String
// let str = "Hello 'Armenia'";
// let str = 'Hello "Armenia"';
// let str = 'Hello \'Armenia\''

// document.write(str);

// let a = 5;
// let b = '5';

// document.writeln(typeof a);
// document.write(typeof b);

// let result = a == b;
// let result = a === b;
// let result = a != b;
// let result = a !== b;

// document.write(result);

// let a = 3;
// let b = 5;
// let text = 'hello';
// let num = '2';

// document.write(a + num);
// document.write(a - num);
// document.write(a * num);
// document.write(a / num);
// document.write(text - a);
// document.write(text * a);
// document.write(text / a);
// document.write(text % a);
// document.write(text + a);

// let result = a + b + text;
// let result = text + a + b;
// let result = text + (a + b);
// let result = '' + a + b;

// let result = text + ' ' + a + ' ' + b;

/* 
String Methods

toLowerCase()
toUpperCase()
charAt()
charCodeAt()
indexOf()
lastIndexOf()
trim()
trimLeft()
trimRight()
trimStart()
trimEnd()
replace()
replaceAll()
concat()
substring()
slice()
substr()
includes()
length
*/

let str = ' Hello';
// document.write(str.toLowerCase());
// document.write(str.toUpperCase());

// document.write(str.charAt(0));
// document.write(str[0]);
// document.write(str.charCodeAt(0))
// document.write('a'.charCodeAt());

// document.write(str.indexOf('a'));
// document.write(str.lastIndexOf('a'));

// document.write(str.trim());
// console.log(str.trim());
// console.log(str.trimLeft());
// console.log(str.trimRight());
// console.log(str.trimStart());
// console.log(str.trimEnd());

// console.log(str.replace('l', 'b'));
// console.log(str.replaceAll('l', 'b'));

// console.log(str.concat(' Hi'));
// console.log(str.concat(' ', 'Hi', ' ', 'Hi '));
// console.log(str + '  ' + 'hjij\"hihihi\"ijii' + ' ' + str);

// console.log(str.substring(1, 3));
// let a = str.substring(1, 3);
// console.log(a);

// console.log(str.slice(1 , 3));
// console.log(str.slice(-2));
// console.log(str.slice(1, -2));

// console.log(str.substr(1, 2));
// console.log(str.includes("el"));

// console.log(str.length);

// console.log(str[0]);

// let st = 'barev';
// let a = st.slice(1);
// let b = a[0].toUpperCase();
// let c = st.slice(2);

// document.write(b.concat(c));

// let string = 'barev';
// string = string.substring(1).replace(string[1], string[1].toLocaleUpperCase());
// document.write(string);

// UI
// alert()
// confirm()
// prompt()

// alert("Hello")

// let res = confirm('Whats up?');
// console.log(res);

// let res = prompt('Whats up?');
// console.log(res);

// window.alert();
// window.confirm();
// window.prompt();

// let a = 10;
// document.write(window.a);

// let value = prompt('Write numbers');
// console.log(
//   value[8] +
//     value[7] +
//     value[6] +
//     value[5] +
//     value[4] +
//     value[3] +
//     value[2] +
//     value[1] +
//     value[0]
// );

// let a = +prompt('Enter number');
// let b = +prompt('Enter number');
// console.log(a + b);

let a = 'hele';
// console.log(a.charAt(1));
// console.log(a.match('ss'));
// console.log(a.search("i"));
// console.log(a.normalize());
// console.log(a.toLocaleLowerCase());
// console.log(a.toLocaleUpperCase());
// let b = a.toString();
// console.log(typeof b);

// console.log(a.repeat(6));

// console.log(a.toLowerCase());
// console.log(a.toLocaleLowerCase());
// console.log(a.startsWith("Բա"))
// console.log(a.indexOf("e"));
// console.log(a.lastIndexOf("e"))
// console.log(a.includes("le"));
