// function func(a, b) {
//   return a + b;
// }

// document.write(func(1, 2));

// const fn = (a, b, c) => {
//   let d = 5;
//   return a * (b + c) + d;
// };

// console.log(fn(2, 3, 4));

// const fn = (a, b, c = '') => {
//   let name = a.toUpperCase();
//   return name + ' ' + b + ' ' + c;
// };

// console.log(fn('yerevan', 'hi'));

// let num = 0;
// function fn(...c) {
//   for (let i = 0; i < c.length; i++) {
//     num += c[i];
//   }
//   return num;
// }
// console.log(c);

// let num = 0;
// function fn() {
//   for (let i = 0; i < arguments.length; i++) {
//     num += arguments[i];
//   }
//   return num;
// }
// console.log(fn(1, 2, 3));

// IIFE Immediately Invoked Function Expression

// (function () {
//   document.write('Hello');
// })();

// const f = (a) => {
//   return function (b) {
//     return (n) => {
//       return a + b + n;
//     };
//   };
// };

// let a = f(3);
// let b = a(4);
// let c = b(2);

// console.log(f(3));

// const f = (a) => {
//   return function (b) {
//     return (n) => {
//       return a + b + n;
//     };
//   };
// };

// console.log(f(3)(2)(3));

// function drawTable(x, width, height, border, bg = '#000') {
//   document.write(`<table width=${width} height=${height} border=${border}> `);
//   for (let i = 0; i < x; i++) {
//     document.write('<tr>');
//     for (let j = 0; j < x; j++) {
//       document.write(`<td bgcolor= ${bg}></td>`);
//     }
//     document.write('</tr>');
//   }
//   document.write('</table>');
// }

// drawTable(6, 153, 150, 2);

let sum = 0;
function calc(arr, simbol) {
  for (let i = 0; i < arr.length; i++) {
    if (typeof arr[i] == 'number') {
      sum = eval(sum + simbol + arr[i]);
    }
  }
  return sum;
}

console.log(calc([1, '2', 3, 4], '+'));
