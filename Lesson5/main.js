// let x = 0;
// x++
// x++
// x++
// x--

// console.log(x);

// let i = 1;
// i++; //postfix
// ++i; // prefix

// let i = 0;
// console.log(i++);
// console.log(i++);

// Loops

// for (let i = 0; i <= 10; i++) {
//   document.writeln(i);
// }

// for (let i = 10; i >= 0; i--) {
//   document.writeln(i);
// }

// for (let i = 0; i <= 100; i++) {
//   if (i % 2 !== 0) {
//     document.writeln(i);
//   }
// }

// for (let i = 0; i <= 100; i++) {
//   if (i >= 25 && i <= 75) {
//     console.log(i);
//   }
// }

// for (let i = 1; i <= 10; i++) {
//   document.write(i + ' x ' + i + '=' + i * i);
//   document.write('<hr>');
// }

// let n = prompt('Write number');

// for (let i = 0; i < 10; i++) {
//   document.writeln(n + '*' + i + ' = ' + n * i);
//   document.writeln('<hr>');
// }

// let digit = prompt('Digit number');
// let str = '';

// for (let i = digit.length - 1; i >= 0; i--) {
//   str += digit[i];
// }

// document.write(str);

//break;
//continue;

// for (let i = 0; i <= 100; i++) {
//   document.writeln(i);
//   if (i == 50) {
//     break;
//   }
// }

// for (let i = 0; i <= 100; i++) {
//   if (i == 50 || i == 52) {
//     continue;
//   }
//   document.writeln(i);
// }

// for (let i = 0; i <= 10; i++) {
//   if (i == 0) {
//     document.write(
//       '<div style= "width:50px ; height: 50px ; background: blue; margin: 2px ; display: inline-block"></div>'
//     );
//   } else if (i == 8) {
//     document.write(
//       '<div style= "width:50px ; height: 50px ; background: red; margin: 2px ; display: inline-block"></div>'
//     );
//   } else if (i == 9) {
//     document.write(
//       '<div style= "width:50px ; height: 50px ; background: green; margin: 2px ; display: inline-block"></div>'
//     );
//   } else if (i == 10) {
//     document.write(
//       '<div style= "width:50px ; height: 50px ; background: black; margin: 2px ; display: inline-block"></div>'
//     );
//   } else {
//     document.write(
//       '<div style= "width:50px ; height: 50px ; background: violet; margin: 2px ; display: inline-block"></div>'
//     );
//   }
// }

// let info = prompt('Write your age');

// if (info < 18) {
//   alert('You are underage');
// } else if (info > 18 && info < 75) {
//   alert('Everithing is good!');
// } else if (info > 75) {
//   alert('Your age is very high');
// } else {
//   alert('Wrong age');
// }

// for (let i = 0; i <= 10; i++) {
//   const size = 50 + i * 10;

//   document.write(
//     `<div style="width: ${size}px; height: ${size}px; background: violet; margin: 2px;"></div>`
//   );
// }

// for (let i = 0; i <= 9; i++) {
//   let size = 10 * i;
//   document.write(
//     `<div style= "width: ${size}px ; height: ${size}px ; background: red ; margin: 2px"></div>
//      <div style= "width: ${size}px ; height: ${size}px ; background: blue ; margin: 2px"></div>
//      <div style= "width: ${size}px ; height: ${size}px ; background: orange ; margin: 2px"></div>`
//   );
// }

// for (let i = 1; i < 9; i++) {
//   let size = 10 * i;
//   document.write(
//     `<div style = "width: ${size}px; height: ${size}px ; background: red;  margin:2px" ></div>

//     <div style = "width: ${size + 10}px ; height: ${
//       size + 10
//     }px ; background: blue ;  margin:2px"></div>
//     <div style = "width: ${size + 20}px ; height: ${
//       size + 20
//     }px ; background: orange ;  margin:2px"></div>
//     `
//   );
// }

// let el;
// for (let i = 1; i <= 10; i++) {
//   if (i == 1) {
//     el =
//       "<div style= 'width:100px ; height: 20px ; background: red; margin :2px'></div>";
//   } else if (i == 4) {
//     el =
//       " <div style = 'width:120px ; height:30px ; background : blue ; margin: 2px'></div>";
//   } else {
//     el =
//       "<div style = 'width:140px; height:40px; background : orange; margin : 3px'></div>";
//   }
//   document.write(el);
// }

// function getRandomColor() {
//   let r = Math.round(Math.random() * 255);
//   let g = Math.round(Math.random() * 255);
//   let b = Math.round(Math.random() * 255);
//   return `rgb(${r},${g},${b})`;
// }

// for (let i = 1; i <= 10; i++) {
//   document.write(
//     `<div style= "width:100px ; height: 100px; background: ${getRandomColor()} ; display: inline-block ; margin: 2px"></div>`
//   );
// }

// for (let i = 1; i <= 10; i++) {
//   document.write(
//     `<div style= "width:100px ; height: 100px; background: rgb(${Math.round(
//       Math.random() * 255
//     )},${Math.round(Math.random() * 255)},${Math.round(
//       Math.random() * 255
//     )}) ; display: inline-block ; margin: 2px"></div>`
//   );
// }

// function getRandomColor() {
//   let r = Math.round(Math.random() * 255);
//   let g = Math.round(Math.random() * 255);
//   let b = Math.round(Math.random() * 255);
//   return `rgb(${r},${g},${b})`;
// }

// for (let i = 1; i <= 10; i++) {
//   let size = 20 * i;

//   document.write(
//     `<div style= "width: ${size}px; height: ${size}px; background:${getRandomColor()}; display: inline-block ; margin:2px"></div>`
//   );
// }

// function printPatern(range) {
//   for (let i = 1; i <= 4; i++) {
//     let str = '';
//     for (let j = 1; j <= i; j++) {
//       str += j + ' ';
//     }
//     console.log(str);
//   }
// }

// printPatern(4);

// let color = 'red';

// for (let i = 0; i < 10; i++) {
//   if (i == 0) {
//     color = 'orange';
//   } else if (i == 9) {
//     color = 'violet';
//   } else {
//     color = 'red';
//   }

//   document.write(
//     '<div style = "width: 30px ; height:30px; margin: 2px; display: inline-block; background:' +
//       color +
//       ' "></div>'
//   );
// }
