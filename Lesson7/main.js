//Array

// let arr = [];
// arr[0] = 10;
// arr[1] = 'Hello';
// arr[2] = true;

// console.log(arr);

// let arr = [10, 'Hello', true];

// let data = [500, 300, 'red', 'green', 'orange', 50];
// document.write(
//   '<div style= "width:' +
//     data[0] +
//     'px ; height:' +
//     data[1] +
//     'px; background:' +
//     data[2] +
//     '; border-radius: ' +
//     data[5] +
//     'px"></div>'
// );

// document.write(
//   `<div style = "width: ${data[0]}px;height: ${data[1]}px ; background: ${data[3]}; border-radius:${data[5]}px"></div>`
// );

// const lenguage = ['HTML', 'CSS', 'JavaScript', 'React'];

// console.log(lenguage);
// console.log(lenguage.length);

// for (let i = 0; i < lenguage.length; i++) {
//   alert(lenguage[i]);
// }

// let emptyData = [];

// for (let i = 0; i < 10; i++) {
//   emptyData[i] = i;
// }

// console.log(emptyData);

// Array Methods

// at();
// fill();
// join();
// sort();
// reverse();
// shift();
// unshift();
// pop();
// push();
// slice();
// indexOf();
// lastIndexOf();
// concat();
// includes();
// splice();
// length;

// let arr = ['hello', 10, true, 'js', 60, 2, false];

// console.log(arr.at(-2))
// console.log(arr.fill('CSS', 2, 4));

// let str = arr.join('  ');
// console.log(str);

// console.log(arr.sort());
// console.log(arr.reverse());
// console.log(arr.shift());
// console.log(arr);
// console.log(arr.unshift('PHP'));
// console.log(arr.pop());

// arr.push('hey', 4, 5)
// console.log(arr);

// let a = arr.slice(2, 4);
// console.log(a);

// let a = arr.indexOf(10);
// let b = arr.lastIndexOf(10);
// console.log(a);
// console.log(b);

// let newArr = ['Light', 'Dark'];
// let res = arr.concat(newArr);
// console.log(res);

// let a = arr.includes('Js');
// console.log(a);

// arr.splice(2, 2);
// arr.splice(2, 0, 'PHP');
// arr.splice(2, 2, 'PHP', 'Laravel');
// console.log(arr);

// let city = ['Yerevan', 'Goris', 'Ararat', 'Masis'];
// let city2 = ['Sevan', 'Gyumri', 'Vardenis', 'Ejmiacin'];

// let a = city.pop();
// city2.unshift(a);
// let b = city.shift();
// city2.push(b);
// let c = city2.at(2);
// city.splice(2, 0, c);
// let e = city.pop();
// city.splice(1, 0, e);
// city.fill('Ijevan', 1, 2);
// let bigArr = city.concat(city2);
// bigArr.reverse();
// let i = bigArr.indexOf('Sevan');
// let o = bigArr.splice(i, 1);
// bigArr.splice(1, 0, o.join());

// console.log(city);
// console.log(city2);
// console.log(bigArr);

// let l = bigArr.indexOf('Gyumri'); //4
// bigArr.splice(3, 0, bigArr.at(4));
// bigArr.splice(l + 1, 1);
// console.log(bigArr);

// bigArr.reverse()
// console.log(bigArr);

// bigArr.push('Yerevan');
// bigArr.splice(8, 1);
// bigArr.splice(0, 1, 'Goris');
// console.log(bigArr);





// let array = [1, 2, 5, 5, 'hello'];

// for (let i = 0; i <= array.length; i++) {
//   if (array[i] != 5) {
//     console.log(array[i]);
//   }
// }

// function calcAge(num) {
//   return num * 365;
// }

// console.log(calcAge(65));

// function cubes(num) {
//   return num ** num;
// }

// console.log(cubes(3));

// function getFirstValue(arr) {
//   return arr[0];
// }

// console.log(getFirstValue([7, 2, 3]));

// function howManySeconds(hour) {
//   return hour * 60 * 60;
// }

// console.log(howManySeconds(2));

// function lessThan100(a, b) {
//   if (a + b > 100) {
//     return false;
//   } else {
//     return true;
//   }
// }

// console.log(lessThan100(12, 12));

// function nameString(string) {
//   return string.concat('Edabit');
// }

// console.log(nameString('Hello'));

// let newArr = [];
// function printArray(num) {
//   for (let i = 1; i < num; i++) {
//     newArr.push(i);
//   }
//   return newArr;
// }

// console.log(printArray(5));
